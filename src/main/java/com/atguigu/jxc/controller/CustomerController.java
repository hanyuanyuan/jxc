package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;


/*
    请求URL：http://localhost:8080 /customer/list
    请求参数：Integer page, Integer rows, String  customerName
    请求方式：POST
    返回值类型：JSON
    返回值：Map<String,Object>
    Response Example Value：
    {
        "total": 3,
            "rows": [
        {
            "customerId": 1,
                "customerName": "家乐福（青石桥店）",
                "contacts": "王二麻子",
                "phoneNumber": "13555555555",
                "address": "成都市锦江区大业路6号COSMO财富中心F3",
                "remarks": "家乐福超市稳定客户"
        }, ……
    ]
*/



    @PostMapping("/list")
    public Map<String,Object>   findCustomerList(Integer page, Integer rows, String customerName ){




        Map<String,Object>  map= customerService.findCustomerList(page,rows,customerName);

        return  map;






    }




 /*   客户添加或修改
   请求URL：http://localhost:8080/ customer/save?customerId=1
    请求参数：
    Customer customer
    请求方式：POST
    返回值类型：JSON
    返回值：ServiceVO
    Response Example Value：
    {
        "code": 100,
            "msg": "请求成功",
            "info": null
    }
*/


    @PostMapping("/save")
    public  ServiceVO  saveOrUpdate(Customer customer,Integer  customerId){
        customerService.saveOrUpdate(customer,customerId);




        ServiceVO  serviceVO=new ServiceVO(100,"请求成功",null);
        return  serviceVO;

    }


/*    2.3、客户删除（支持批量删除）
    请求URL：http://localhost:8080/customer/delete
    请求参数：String  ids
    请求方式：POST
    返回值类型：JSON
    返回值：ServiceVO
    Response Example Value：
    {
        "code": 100,
            "msg": "请求成功",
            "info": null
    }*/

    @PostMapping("/delete")
    public   ServiceVO  delete(String  ids){
        customerService.delete(ids);



        ServiceVO  serviceVO=new ServiceVO(100,"请求成功",null);
        return  serviceVO;



    }




}
