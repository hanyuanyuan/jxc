package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.DamageListService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListService  damageListService;

    @Autowired
    private DamageListGoodsService  damageListGoodsService;




   /* 请求URL：http://localhost:8080/damageListGoods/save?damageNumber=BS1605766644460（报损单号,前端生成）
    请求参数：
    DamageList damageList, String damageListGoodsStr
    请求方式：POST
    返回值类型：JSON
    返回值：ServiceVO
    Response Example Value：
    {
        "code": 100,
            "msg": "请求成功",
            "info": null
    }
    */

    @PostMapping(value = "/save")
    public  ServiceVO  save(DamageList damageList, String damageListGoodsStr, HttpSession session){




        damageListService.save(damageList,session);
        Integer damageListId = damageList.getDamageListId();
        System.out.println(damageListId+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");//null




        Gson gson=new Gson();
        List<DamageListGoods> damageListGoodsList1=gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>(){}.getType());

        List<DamageListGoods> damageListGoodsList = damageListGoodsList1.stream().map(damageListGoods -> {
            damageListGoods.setDamageListId(damageListId);
            return damageListGoods;
        }).collect(Collectors.toList());


        damageListGoodsService.save(damageListGoodsList);


        ServiceVO  serviceVO=new ServiceVO(100,"请求成功",null);
        return  serviceVO;
    }

  /*  4.1、报损单查询
    请求URL：http://localhost:8080/damageListGoods/list
    请求参数：String  sTime（开始时间）, String  eTime（结束时间）
    请求方式：POST
    返回值类型：JSON
    返回值：Map<String,Object>
    Response Example Value：
    {
        "rows": [
        {
            "damageListId": 3,
                "damageNumber": "BS1605766247962",
                "damageDate": "2020-11-19",
                "remarks": "",
                "userId": 1,
                "trueName": "兰杰"
        }, ………
    ]*/

    @PostMapping(value = "/list")
    public Map<String,Object> list(String  sTime, String  eTime,HttpSession  session){


        Map<String,Object> map=new HashMap<>();
        List<DamageList>  damageLists=damageListService.findByTime(sTime,eTime,session);
        map.put("rows",damageLists);
        return  map;

    }


    /*4.2、查询报损单商品信息
    请求URL：http://localhost:8080/damageListGoods/goodsList
    请求参数：Integer damageListId（报损单Id）
    请求方式：POST
    返回值类型：JSON
    返回值：Map<String,Object>
    Response Example Value：
    {
        "rows": [
        {
            "damageListGoodsId": 3,
                "goodsId": 12,
                "goodsCode": "0004",
                "goodsName": "新疆红枣",
                "goodsModel": "2斤装",
                "goodsUnit": "袋",
                "goodsNum": 2,
                "price": 13,
                "total": 26,
                "damageListId": 3,
                "goodsTypeId": 10
        },…………
    ]
    }*/
    @PostMapping(value = "/goodsList")
    public Map<String,Object>  goodsList(Integer damageListId){

        Map<String,Object> map= damageListGoodsService.goodsList(damageListId);
        return  map;

    }









}
