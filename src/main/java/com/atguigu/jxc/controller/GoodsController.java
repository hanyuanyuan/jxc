package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;


import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @description 商品信息Controller
 */


@RestController
@RequestMapping(value = "/goods")
public class GoodsController {


    //Map<String,Object>







    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */

    @PostMapping(value = "/listInventory")

    public Map<String,Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId){
       Map<String,Object>  map= goodsService.listInventory(page,rows,codeOrName,goodsTypeId);



        return  map;





    }


    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
/*
    3.3、查询所有商品信息（可      家具家电及诶诶你们的额死你们以根据分类、名称查询）
    请求URL：http://localhost:8080/goods/list
    请求参数：Integer page, Integer rows, String goodsName, Integer goodsTypeId
    请求方式：POST
    返回值类型：JSON
    返回值：Map<String,Object>
    Response Example Value：
    {
        "total": 26,
            "rows": [
        {
            "goodsId": 1,
                "goodsCode": "0001",
                "goodsName": "陶华碧老干妈香辣脆油辣椒",
                "inventoryQuantity": 167,
                "lastPurchasingPrice": 8.5,
                "minNum": 1000,
                "goodsModel": "红色装",
                "goodsProducer": "贵州省贵阳南明老干妈风味食品有限公司",
                "purchasingPrice": 7.95,
                "remarks": "好卖",
                "sellingPrice": 8.5,
                "state": 2,
                "goodsUnit": "瓶",
                "goodsTypeId": 10,
                "goodsTypeName": "地方特产",
                "saleTotal": null
        }, …………
    ]
    }
*/


    @PostMapping(value = "/list")
    public Map<String,Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId){
        Map<String,Object>  map= goodsService.list(page,rows,goodsName,goodsTypeId);
        return  map;
    }
















    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */
   /* 3.6、商品添加或修改
    请求URL：http://localhost:8080/goods/save?goodsId=37
    请求参数：Goods goods
    请求方式：POST
    返回值类型：JSON
    返回值：ServiceVO
    Response Example Value：
    {
        "code": 100,
            "msg": "请求成功",
            "info": null
    }*/

    @PostMapping("/save")
    public  ServiceVO  saveOrUpdate(Goods goods,Integer  goodsId){
        goodsService.saveOrUpdate(goods,goodsId);
        ServiceVO  serviceVO=new ServiceVO(100,"请求成功",null);
        return  serviceVO;

    }




    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */

  /*
    3.7、商品删除（要判断商品状态,入库、有进货和销售单据的不能删除）
    请求URL：http://localhost:8080/goods/delete
    请求参数：Integer goodsId
    请求方式：POST
    返回值类型：JSON
    返回值：ServiceVO
    Response Example Value：
    {
        "code": 100,
            "msg": "请求成功",
            "info": null
            0表示初始值,1表示已入库，2表示有进货或销售单据
    }*/
    @PostMapping(value = "/delete")

    public ServiceVO delete(Integer goodsId){
        Integer  state= goodsService.findGoodsStateById(goodsId);
       if(state==0){
           goodsService.delete(goodsId);

           ServiceVO  serviceVO=new ServiceVO(100,"请求成功",null);
           return  serviceVO;


       }else {

           ServiceVO  serviceVO=new ServiceVO(103,"请求失败，已入库、有进货或销售单据不能删除",null);
           return  serviceVO;

       }

    }







    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */

  /*  4.1、无库存商品列表展示（可以根据商品名称或编码查询）
    请求URL：http://localhost:8080/goods/getNoInventoryQuantity
    请求参数：Integer page,Integer rows,String nameOrCode
    请求方式：POST
    返回值类型：JSON
    返回值：Map<String,Object>
    Response Example Value：
    {
        "total": 8,

    ]
    }*/
    @PostMapping("/getNoInventoryQuantity")
    public Map<String,Object>  getNoInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        Map<String,Object> map=goodsService.getNoInventoryQuantity(page,rows,nameOrCode);
        return  map;

    }


    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
   /*
    4.2、有库存商品列表展示（可以根据商品名称或编码查询）
    请求URL：http://localhost:8080/goods/getHasInventoryQuantity
    请求参数：Integer page,Integer rows,String nameOrCode
    请求方式：POST
    返回值类型：JSON
    返回值：Map<String,Object>
    Response Example Value：
    {
        "total": 18,
            "rows": [
        {*/
    @PostMapping("/getHasInventoryQuantity")
    public Map<String,Object>  getHasInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        Map<String,Object> map=goodsService.getHasInventoryQuantity(page,rows,nameOrCode);
        return  map;

    }



    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */

 /*
    4.3、添加库存、修改数量或成本价
    请求URL：http://localhost:8080/goods/saveStock?goodsId=25
    请求参数：Integer goodsId,Integer inventoryQuantity,double purchasingPrice
    请求方式：POST
    返回值类型：JSON
    返回值：ServiceVO
    Response Example Value：
    {
        "code": 100,
            "msg": "请求成功",
            "info": null
    }*/

    @PostMapping("/saveStock")
    public  ServiceVO  saveStock(Integer goodsId,Integer inventoryQuantity,double purchasingPrice){

        goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);





        ServiceVO  serviceVO=new ServiceVO(100,"请求成功",null);
        return  serviceVO;
    }




    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */

    /**
     * 查询库存报警商品信息
     * @return
     */

    /*
    4.4、删除库存（要判断商品状态 入库、有进货和销售单据的不能删除）
    请求URL：http://localhost:8080/goods/deleteStock
    请求参数：Integer goodsId
    请求方式：POST
    返回值类型：JSON
    返回值：ServiceVO
    Response Example Value：
    {
        "code": 100,
            "msg": "请求成功",
            "info": null
    }
    */

    @PostMapping("/deleteStock")
    public  ServiceVO  deleteStock(Integer goodsId){

        Integer  state= goodsService.findGoodsStateById(goodsId);
        if(state==0){
            goodsService.deleteStock(goodsId);

            ServiceVO  serviceVO=new ServiceVO(100,"请求成功",null);
            return  serviceVO;


        }else {

            ServiceVO  serviceVO=new ServiceVO(103,"请求失败，已入库、有进货或销售单据不能删除",null);
            return  serviceVO;

        }




    }
    /*查询所有 当前库存量 小于 库存下限的商品信息
    请求URL：http://localhost:8080/goods/listAlarm
    请求参数：无
    请求方式：POST
    返回值类型：JSON
    返回值：Map<String,Object>
    Response Example Value：
    {
        "rows": [
        {
*/
    @PostMapping("/listAlarm")
    public Map<String,Object>  listAlarm(){
        Map<String,Object> map= goodsService.listAlarm();
        return map;


    }








































}
