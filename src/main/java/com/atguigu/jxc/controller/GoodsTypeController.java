package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.service.GoodsTypeService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * @description 商品类别控制器
 */
@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    /**
     * 查询所有商品类别
     * @return easyui要求的JSON格式字符串
     */
    @PostMapping("/loadGoodsType")
    @RequiresPermissions(value={"商品管理","进货入库","退货出库","销售出库","客户退货","当前库存查询","商品报损","商品报溢","商品采购统计"},logical = Logical.OR)
    public ArrayList<Object> loadGoodsType() {
        return goodsTypeService.loadGoodsType();
    }



/*3.4、新增分类
    请求URL：http://localhost:8080/goodsType/save
    请求参数：String  goodsTypeName,Integer  pId
    请求方式：POST
    返回值类型：JSON
    返回值：ServiceVO
    Response Example Value：
    {
        "code": 100,
            "msg": "请求成功",
            "info": null
    }*/

@PostMapping("/save")
    public  ServiceVO  save(String  goodsTypeName,Integer  pId){
        goodsTypeService.save(goodsTypeName,pId);





        ServiceVO  serviceVO=new ServiceVO(100,"请求成功",null);
        return  serviceVO;






    }

   /* 3.5、删除分类
    请求URL：http://localhost:8080/goodsType/delete
    请求参数：Integer  goodsTypeId
    请求方式：POST
    返回值类型：JSON
    返回值：ServiceVO
    Response Example Value：
    {
        "code": 100,
            "msg": "请求成功",
            "info": null
    }*/


    @PostMapping("/delete")
    public ServiceVO  delete(Integer  goodsTypeId){
        goodsTypeService.delete(goodsTypeId);


        ServiceVO  serviceVO=new ServiceVO(100,"请求成功",null);
        return  serviceVO;
    }


















}
