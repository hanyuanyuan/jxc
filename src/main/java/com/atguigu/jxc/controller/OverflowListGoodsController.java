package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.atguigu.jxc.service.OverflowListService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {


    @Autowired
    private OverflowListService  overflowListService;

    @Autowired
    private OverflowListGoodsService  overflowListGoodsService;




   /* 2.1、新增报溢单
 2.1、新增报溢单
请求URL：http://localhost:8080/overflowListGoods/save?overflowNumber=BY1605767033015（报溢单号）
请求参数：OverflowList overflowList, String overflowListGoodsStr
请求方式：POST
返回值类型：JSON
返回值：ServiceVO
Response Example Value：
{
    "code": 100,
    "msg": "请求成功",
    "info": null
}
    }*/

    @PostMapping("/save")
    @Transactional
    public ServiceVO  save(  OverflowList overflowList, String overflowListGoodsStr,HttpSession  session){



        overflowListService.save(overflowList,session);
        Integer overflowListId = overflowList.getOverflowListId();
        System.out.println(overflowListId+"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");//null




        Gson gson=new Gson();
        List<OverflowListGoods> overflowListGoodsList=gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>(){}.getType());

        List<OverflowListGoods> overflowListGoods1 = overflowListGoodsList.stream().map(overflowListGoods -> {
            overflowListGoods.setOverflowListId(overflowListId);
            return overflowListGoods;
        }).collect(Collectors.toList());


        overflowListGoodsService.save(overflowListGoods1);


        ServiceVO  serviceVO=new ServiceVO(100,"请求成功",null);
        return  serviceVO;

    }

















 /*   4.3、报溢单查询
    请求URL：http://localhost:8080/overflowListGoods/list
    请求参数：String  sTime（开始时间）, String  eTime（结束时间）
    请求方式：POST
    返回值类型：JSON
    返回值：Map<String,Object>
    Response Example Value：
    {
        "rows": [
        {
            "overflowListId": 3,
                "overflowNumber": "BY1605766414384",
                "overflowDate": "2020-11-19",
                "remarks": "多了",
                "userId": 1,
                "trueName": "兰杰"
        }, …………
    ]
    }*/

    @PostMapping(value = "/list")

    public Map<String,Object> list(String  sTime, String  eTime, HttpSession  session){

        Map<String,Object> map=overflowListService.list(sTime,eTime,session);

        return  map;


    }


  /*  4.4、报溢单商品信息
    请求URL：http://localhost:8080/overflowListGoods/goodsList
    请求参数：Integer overflowListId
    请求方式：POST
    返回值类型：JSON
    返回值：Map<String,Object>
    Response Example Value：
    {
        "rows": [
        {
            "damageListGoodsId": 3,
                "goodsId": 12,
                "goodsCode": "0004",
                "goodsName": "新疆红枣",
                "goodsModel": "2斤装",
                "goodsUnit": "袋",
                "goodsNum": 2,
                "price": 13,
                "total": 26,
                "damageListId": 3,
                "goodsTypeId": 10
        },…………
    ]
    }*/


    @PostMapping(value = "/goodsList")
    public  Map<String,Object>  goodsList(Integer overflowListId){


        Map<String,Object> map=   overflowListGoodsService.goodsList(overflowListId);
        return map;

    }















}
