package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/supplier")
public class SupplierController {



    @Autowired
    private SupplierService  supplierService;


    @PostMapping("/list")
    public Map<String,Object> supplierList(Integer page, Integer rows, String supplierName){

        Map<String,Object>   map= supplierService.supplierList(page,rows,supplierName);
        return  map;


    }


    /*
    *
    * 1.2、供应商添加或修改
     请求URL：http://localhost:8080/supplier/save?supplierId=1
     请求参数：Supplier supplier
     请求方式：POST
     返回值类型： JSON
     返回值：ServiceVO
     Response Example Value：
      {
          "code": 100,
          "msg": "请求成功",
          "info": null
      }
    *
    * */

    @PostMapping(value = "/save")
    public ServiceVO  saveOrUpdateSupplier( Integer supplierId, Supplier supplier){
        supplierService.saveOrUpdateSupplier(supplierId,supplier);


        ServiceVO serviceVO=new ServiceVO(100,"请求成功",null);
        return serviceVO;
    }



/*
    1.3、删除供应商（支持批量删除）
    请求URL：http://localhost:8080/supplier/delete
    请求参数：String  ids
    请求方式：POST
    返回值类型：JSON
    返回值：ServiceVO
    Response Example Value：
    {
        "code": 100,
            "msg": "请求成功",
            "info": null
    }
*/



    @PostMapping("/delete")
    public  ServiceVO  deleteSupplier(String  ids){

        supplierService.deleteSupplier(ids);



        ServiceVO serviceVO=new ServiceVO(100,"请求成功",null);



        return serviceVO;
    }






}
