package com.atguigu.jxc.controller;


import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/unit")
public class UnitController {

    @Autowired
    private UnitService  unitService;


/*    请求URL：http://localhost:8080/unit/list
    请求参数：无
    请求方式：POST
    返回值类型：JSON
    返回值：Map<String,Object>
    Response Example Value：
    {
        "rows": [
        {
            "unitId": 1,
                "unitName": "元"
        },
        {
            "unitId": 6,
                "unitName": "户"
        }, ………
    ]
    }*/

    @PostMapping("/list")
    public Map<String,Object> list(){
        Map<String,Object>  map=unitService.list();
        return  map;




    }








}
