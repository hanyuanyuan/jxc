package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {

    List<Customer> findCustomerList(@Param("offset") Integer offset, @Param("rows") Integer rows, @Param("customerName") String customerName);

    Object finduCstomerCount();

    void update(@Param("customer") Customer customer, @Param("customerId") Integer customerId);

    void save(@Param("customer") Customer customer);

    void delete(@Param("integerList") List<Integer> integerList);
}
