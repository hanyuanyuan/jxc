package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单
 */
public interface DamageListGoodsDao {


    void save(@Param("damageListGoodsList") List<DamageListGoods> damageListGoodsList);

    List<DamageListGoods> goodsList(Integer damageListId);
}
