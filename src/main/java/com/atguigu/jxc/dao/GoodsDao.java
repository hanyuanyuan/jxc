package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();




    List<Goods> getGoodsInventoryList(int offSet, Integer rows, @Param("goods")  Goods goods);

    Object getGoodsInventoryCount( @Param("goods") Goods goods, Integer goodsTypeId);

    List<Goods> getList(int offSet, Integer rows, Goods goods);

    void update(@Param("goods") Goods goods, @Param("goodsId") Integer goodsId);

    void save(@Param("goods") Goods goods);



    void delete(Integer goodsId);

    Integer findGoodsStateById(Integer goodsId);

    Object getNoInventoryTotal();

    List<Goods> getNoInventoryList(int offSet, Integer rows,  @Param("goods")Goods goods);

    List<Goods> getInventoryList(int offSet, Integer rows, Goods goods);

    Object getInventoryTotal();

    void saveStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity,@Param("purchasingPrice") double purchasingPrice);





    void updateStock(Integer goodsId);

    List<Goods> listAlarm();
}
