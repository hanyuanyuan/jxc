package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;

import java.util.List;

public interface OverflowListDao {
    List<OverflowList> list(String sTime, String eTime);

    void save(OverflowList overflowList);
}
