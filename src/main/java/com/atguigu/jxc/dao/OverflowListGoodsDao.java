package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListGoodsDao {
    List<OverflowListGoods> goodsList(Integer overflowListId);

    void save(@Param("overflowListGoods1") List<OverflowListGoods> overflowListGoods1);
}
