package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {
    List<Supplier> getSupplierList(@Param("offSet")int offSet,@Param("rows") Integer rows, @Param("supplierName") String supplierName);

    Object getSupplierCount();

    void update(@Param("supplierId") Integer supplierId, @Param("supplier") Supplier supplier);

    void save(@Param("supplier") Supplier supplier);

    void deleteSupplier(@Param("integerList") List<Integer> integerList);
}
