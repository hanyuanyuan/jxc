package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;
import java.util.Map;

public interface DamageListGoodsService {
    void save(List<DamageListGoods> damageListGoodsList);

    Map<String, Object> goodsList(Integer damageListId);
}
