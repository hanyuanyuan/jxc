package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface DamageListService {
    void save(DamageList damageList, HttpSession session);

    List<DamageList> findByTime(String sTime, String eTime,HttpSession  session);
}
