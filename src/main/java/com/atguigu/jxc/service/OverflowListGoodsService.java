package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;
import java.util.Map;

public interface OverflowListGoodsService {
    Map<String, Object> goodsList(Integer overflowListId);


    void save(List<OverflowListGoods> overflowListGoods1);
}
