package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface OverflowListService {
    Map<String, Object> list(String sTime, String eTime, HttpSession session);

    void save(OverflowList overflowList, HttpSession session);
}
