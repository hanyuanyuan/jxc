package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    Map<String, Object> supplierList(Integer page, Integer rows, String supplierName);

    void saveOrUpdateSupplier(Integer supplierId, Supplier supplier);

    void deleteSupplier(String ids);
}
