package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao   customerDao;
    @Override
    public Map<String, Object> findCustomerList(Integer page, Integer rows, String customerName) {
        Map<String,Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offset = (page - 1) * rows;

       List<Customer> customerList= customerDao.findCustomerList(offset,rows,customerName);

       map.put("rows",customerList);
       map.put("total",customerDao.finduCstomerCount());

        return map;
    }

    @Override
    public void saveOrUpdate(Customer customer, Integer customerId) {


        if(customerId !=null){
            //有id就是更新
            customerDao.update(customer,customerId);

        }else {
            //没有id就是保存
            customerDao.save(customer);

        }

    }

    @Override
    public void delete(String ids) {

        List<String> idsList = Arrays.asList(ids.split(","));


        List<Integer> integerList = idsList.stream().map(id -> Integer.parseInt(id)).collect(Collectors.toList());

        //批量删除
        customerDao.delete(integerList);


    }
}
