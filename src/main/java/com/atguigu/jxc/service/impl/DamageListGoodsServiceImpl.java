package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private DamageListGoodsDao  damageListGoodsDao;
    @Override
    public void save(List<DamageListGoods> damageListGoodsList) {

        damageListGoodsDao.save(damageListGoodsList);

    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String, Object> map=new HashMap<>();
      List<DamageListGoods> damageListGoodsList=  damageListGoodsDao.goodsList(damageListId);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+damageListGoodsList);




        map.put("rows",damageListGoodsList);






        return map;
    }


}
