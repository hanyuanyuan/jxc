package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.config.MyRealm;
import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DamageListServiceImpl  implements DamageListService {

    @Autowired
    private DamageListDao  damageListDao;
    @Override
    public void save(DamageList damageList, HttpSession  session) {
        User user = (User)session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        damageList.setUserId(userId);
        damageListDao.save(damageList);




    }


    public List<DamageList> findByTime(String sTime, String eTime,HttpSession  session) {
        User user = (User)session.getAttribute("currentUser");


        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date startTime = format.parse(sTime);
            Date endTime = format.parse(eTime);
            List<DamageList>  damageLists=damageListDao.findByTime(startTime,endTime);


            List<DamageList> damageListList = damageLists.stream().map(damageList -> {
                damageList.setTrueName(user.getTrueName());
                return damageList;
            }).collect(Collectors.toList());



            return damageListList;



        } catch (ParseException e) {
            throw new RuntimeException(e);
        }



    }
}
