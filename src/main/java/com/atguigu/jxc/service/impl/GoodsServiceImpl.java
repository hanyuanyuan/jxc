package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;


    @Autowired
    private  CustomerReturnListGoodsService  customerReturnListGoodsService;

    @Autowired
    private   SaleListGoodsService   saleListGoodsService;




    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }


  @Override
  public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
      Map<String,Object> map = new HashMap<>();

      page = page == 0 ? 1 : page;
      int offSet = (page - 1) * rows;
      Goods goods=new Goods();

      if(codeOrName !=null){
          byte[] bytes = codeOrName.getBytes();
          if(bytes[0]==0){
              goods.setGoodsCode(codeOrName);

          }else {
              goods.setGoodsName(codeOrName);
          }


      }
      goods.setGoodsTypeId(goodsTypeId);

      List<Goods> goodsList = goodsDao.getGoodsInventoryList(offSet, rows, goods);


  /*    //@Autowired
          private  CustomerReturnListGoodsService  customerReturnListGoodsService;

         @Autowired
         private   SaleListGoodsService  saleListGoodsService;*/

    /*  for (Goods goods : goodsList) {
          // 销售总量等于销售单据的销售数据减去退货单据的退货数据
          goods.setSaleTotal(saleListGoodsService.getSaleTotalByGoodsId(goods.getGoodsId())
                  - customerReturnListGoodsService.getCustomerReturnTotalByGoodsId(goods.getGoodsId()));

      }*/


      for (Goods goods1 : goodsList) {
          // 销售总量等于销售单据的销售数据减去退货单据的退货数据



        /*  goods.setSaleTotal(saleListGoodsService.getSaleNum(goods.getGoodsId())-customerReturnListGoodsService.getReturnNum(goods1.getGoodsId()));*/



          Integer saleNum = saleListGoodsService.getSaleNum(goods1.getGoodsId());


          Integer returnNum = customerReturnListGoodsService.getReturnNum(goods1.getGoodsId());


          goods1.setSaleTotal(saleNum-returnNum);




      }

      map.put("rows", goodsList);

      map.put("total", goodsDao.getGoodsInventoryCount(goods, goodsTypeId));



      return map;
  }

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {


        Map<String,Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        Goods goods=new Goods();
        goods.setGoodsName(goodsName);
        goods.setGoodsTypeId(goodsTypeId);
        List<Goods> goodsList = goodsDao.getList(offSet, rows, goods);

        for (Goods goods1 : goodsList) {
            // 销售总量等于销售单据的销售数据减去退货单据的退货数据

            /*  goods.setSaleTotal(saleListGoodsService.getSaleNum(goods.getGoodsId())-customerReturnListGoodsService.getReturnNum(goods1.getGoodsId()));*/



            Integer saleNum = saleListGoodsService.getSaleNum(goods1.getGoodsId());


            Integer returnNum = customerReturnListGoodsService.getReturnNum(goods1.getGoodsId());


            goods1.setSaleTotal(saleNum-returnNum);




        }
        map.put("rows", goodsList);

        map.put("total", goodsDao.getGoodsInventoryCount(goods, goodsTypeId));

        return map;
    }

    @Override
    public void saveOrUpdate(Goods goods, Integer goodsId) {
        if(goodsId !=null){
            //修改
            goodsDao.update(goods,goodsId);



        }else {
            //新增
            goodsDao.save(goods);

        }















    }



    @Override
    public void delete(Integer goodsId) {
        goodsDao.delete(goodsId);

    }

    @Override
    public Integer findGoodsStateById(Integer goodsId) {

        Integer  sta=  goodsDao.findGoodsStateById(goodsId);
        return sta;
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String,Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        Goods goods=new Goods();

        if(nameOrCode !=null){
            byte[] bytes = nameOrCode.getBytes();
            if(bytes[0]==0){
                goods.setGoodsCode(nameOrCode);

            }else {
                goods.setGoodsName(nameOrCode);
            }


        }

        List<Goods> goodsList = goodsDao.getNoInventoryList(offSet, rows, goods);



        for (Goods goods1 : goodsList) {



            goods1.setSaleTotal(null);

        }

        map.put("rows",goodsList);

        map.put("total", goodsDao.getNoInventoryTotal());
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String,Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        Goods goods=new Goods();

        if(nameOrCode !=null){
            byte[] bytes = nameOrCode.getBytes();
            if(bytes[0]==0){
                goods.setGoodsCode(nameOrCode);

            }else {
                goods.setGoodsName(nameOrCode);
            }


        }

        List<Goods> goodsList = goodsDao.getInventoryList(offSet, rows, goods);



        for (Goods goods1 : goodsList) {



            goods1.setSaleTotal(null);

        }

        map.put("rows",goodsList);

        map.put("total", goodsDao.getInventoryTotal());
        return map;
    }

    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.saveStock(goodsId,inventoryQuantity,purchasingPrice);
    }

    @Override
    public void deleteStock(Integer goodsId) {
        //删除库存
        goodsDao.updateStock(goodsId);





    }

    @Override
    public Map<String, Object> listAlarm() {

        // 当前库存量 小于 库存下限的商品信息
        Map<String,Object> map = new HashMap<>();
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

        List<Goods> goodsList= goodsDao.listAlarm();

        for (Goods goods1 : goodsList) {



            goods1.setSaleTotal(null);

        }
        map.put("rows",goodsList);
        System.out.println(goodsList+"--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");



        return map;
    }

}
