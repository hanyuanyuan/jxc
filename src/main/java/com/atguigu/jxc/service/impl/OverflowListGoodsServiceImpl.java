package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListGoodsServiceImpl  implements OverflowListGoodsService {

    @Autowired
    private OverflowListGoodsDao  overflowListGoodsDao;
    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {


        Map<String, Object>  map=new HashMap<>();
      List<OverflowListGoods> overflowListGoodsList= overflowListGoodsDao.goodsList(overflowListId);


        map.put("rows",overflowListGoodsList);
        return map;
    }

    @Override
    public void save(List<OverflowListGoods> overflowListGoods1) {
        overflowListGoodsDao.save(overflowListGoods1);
    }

}
