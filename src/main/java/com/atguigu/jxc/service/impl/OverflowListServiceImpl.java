package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverflowListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class OverflowListServiceImpl implements OverflowListService {

    @Autowired
    private OverflowListDao  overflowListDao;
    @Override
    public Map<String, Object> list(String sTime, String eTime, HttpSession session) {
        User user = (User)session.getAttribute("currentUser");
        String trueName = user.getTrueName();


        Map<String, Object> map=new HashMap<>();

       List<OverflowList> overflowListList= overflowListDao.list(sTime,eTime);


        List<OverflowList> overflowLists = overflowListList.stream().map(overflowList -> {
            overflowList.setTrueName(trueName);
            return overflowList;
        }).collect(Collectors.toList());

        map.put("rows",overflowLists);


        return map;
    }

    @Override
    public void save(OverflowList overflowList, HttpSession session) {

        User user = (User)session.getAttribute("currentUser");
        Integer userId = user.getUserId();
        overflowList.setUserId(userId);
        overflowListDao.save(overflowList);

    }
}
