package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class SupplierServiceImpl implements SupplierService {


    @Autowired
    private SupplierDao  supplierDao;
    @Override
    public Map<String, Object> supplierList(Integer page, Integer rows, String supplierName) {
        Map<String,Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;

        int offSet = (page - 1) * rows;

        List<Supplier> supplierList = supplierDao.getSupplierList(offSet, rows, supplierName);


        map.put("rows", supplierList);

        map.put("total", supplierDao.getSupplierCount());



        return map;
    }

    @Override
    @Transactional
    public void saveOrUpdateSupplier(Integer supplierId, Supplier supplier) {


        if(supplierId !=null){
            //有id就是更新
            supplierDao.update(supplierId,supplier);




        }else {
            //没有id就是保存
            supplierDao.save(supplier);

        }

    }

    @Override
    public void deleteSupplier(String ids) {

        List<String> idsList = Arrays.asList(ids.split(","));

        List<Integer> integerList = idsList.stream().map(id -> Integer.parseInt(id)).collect(Collectors.toList());


        //批量删除
        supplierDao.deleteSupplier(integerList);


    }
}
